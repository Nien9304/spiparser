TARGET = ../spiParser
OBJECT = parser.tab.o lex.yy.o main.o common.o
OUTPUT = lex.yy.c parser.tab.c parser.tab.h
LEX=flex
YACC=bison
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
#makefile knows the dependency between .o .cpp
struct.h: common.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp

main.o:main.cpp parser.tab.h struct.h
	$(CC) $(CCFLAG) -c main.cpp
lex.yy.o:lex.yy.c parser.tab.h struct.h
	$(CC) $(CCFLAG) -c lex.yy.c
parser.tab.o:parser.tab.c struct.h
	$(CC) $(CCFLAG) -c parser.tab.c
parser.tab.c parser.tab.h:parser.y struct.h
	$(YACC) -d parser.y
lex.yy.c:lexer.l struct.h
	$(LEX) lexer.l
clean:
	rm -f $(TARGET) $(OBJECT) $(OUTPUT)