#!/usr/bin/python
from __future__ import print_function
import itertools
import sys
import os
target_list = ['7c500t',]
#target_list = ['dual_USBPHY_20','dual_USB3PHY_20']
job_list = [('1tf',(
	('ca','sa'),
	('ca','br'),
	('ca','sa+br'),
	('sa','ca'),
	)), ('2tf',(
	('ca','tr'),
	('tr','ca')))]
rpt_path = 'rpt/compare_type'
filename_format = '.{target}.{model_type}.rpt.rpt'
raw_filename_format_rpt = rpt_path + '.raw' + filename_format
grouped_filename_format_rpt = rpt_path + '.{{group_name}}' + filename_format
filename_format = '.{target}.{model_type}.cmp.rpt'
raw_filename_format_cmp = rpt_path + '.raw' + filename_format
grouped_filename_format_cmp = rpt_path + '.{{group_name}}' + filename_format
filename_format = '.{target}.{model_type}.cmp_be.rpt'
raw_filename_format_cmp_be = rpt_path + '.raw' + filename_format
grouped_filename_format_cmp_be = rpt_path + '.{{group_name}}' + filename_format

def write_scripts():
	with open('auto_generated_scripts/script_compare_type.txt','w') as output_file:
		sys.stdout = output_file
		print('source add_group.tcl')
		print('#Script defaults: {group_name}')
		for target in target_list:
			print('clear_target')
			print('load_target {0}'.format(target))
			print('gen_pattern')
			print('analyze_pattern')
			print('gen_1tf_ca_model 60 -name ca')
			print('gen_2tf_ca_model 0.5 -name ca -1tf_model ca')
			print('gen_1tf_sa_model -name sa')
			print('gen_1tf_bridge_model -name br')
			print('add_1tf_model sa+br sa br')
			print('gen_2tf_tr_model -name tr')
			# report
			for model_type, model_list in [('1tf',('ca','sa','br','sa+br')),('2tf',('ca','tr'))]:
				raw_filename = raw_filename_format_rpt.format(
					target=target, model_type=model_type)
				grouped_filename = grouped_filename_format_rpt.format(
					target=target, model_type=model_type)
				print('echo > {0}'.format(raw_filename))
				print('echo > {0}'.format(grouped_filename))
				for model in model_list:
					gen_report_commands(output_file, raw_filename, grouped_filename, 
														model_type, model)
			# compare
			for model_type, model_list in job_list:
				raw_filename = raw_filename_format_cmp.format(
					target=target, model_type=model_type)
				grouped_filename = grouped_filename_format_cmp.format(
					target=target, model_type=model_type)
				raw_filename_be = raw_filename_format_cmp_be.format(
					target=target, model_type=model_type)
				grouped_filename_be = grouped_filename_format_cmp_be.format(
					target=target, model_type=model_type)
				print('echo > {0}'.format(raw_filename))
				print('echo > {0}'.format(grouped_filename))
				print('echo > {0}'.format(raw_filename_be))
				print('echo > {0}'.format(grouped_filename_be))
				for model_A, model_B in model_list:
					gen_comaprison_commands(output_file, raw_filename, grouped_filename, 
					raw_filename_be, grouped_filename_be, model_type, model_A, model_B)

					
def gen_report_commands(output_file, raw_filename, grouped_filename, 
														model_type, model):
	# report model
	report_name='rpt_{model_type}_{model}'.format(
		model_type=model_type, model=model)
	command = 'report_{model_type}_model {model} \
-report {report_name} -group {{group_name}}'.format(
		model_type=model_type, model=model, report_name=report_name)
	print(command)
	#print('echo {1} >> {0}'.format(raw_filename, '#'*80))
	print('echo {1} >> {0}'.format(raw_filename, command))
	print('show_report {report_name} >> {filename}'.format(
		filename=raw_filename, report_name=report_name))
		
	#print('echo {1} >> {0}'.format(grouped_filename, '#'*80))
	print('echo {1} >> {0}'.format(grouped_filename, command))
	print('show_grouped_report {report_name} -group {{group_name}} \
>> {filename}'.format(
		filename=grouped_filename, report_name=report_name))

def gen_comaprison_commands(output_file, raw_filename, grouped_filename, 
		raw_filename_be, grouped_filename_be, model_type, model_A, model_B):
	# compare model
	report_name='cmp_{model_type}_{model_A}-{model_B}'.format(
		model_type=model_type, model_A=model_A, model_B=model_B)
	command = 'compare_{model_type}_model {model_A} {model_B} \
-report {report_name} -group {{group_name}}'.format(
		model_type=model_type, model_A=model_A, model_B=model_B, 
		report_name=report_name)
	print(command)
	#print('echo {1} >> {0}'.format(raw_filename, '#'*80))
	print('echo {1} >> {0}'.format(raw_filename, command))
	print('show_report {report_name} >> {filename}'.format(
		filename=raw_filename, report_name=report_name))
		
	#print('echo {1} >> {0}'.format(grouped_filename, '#'*80))
	print('echo {1} >> {0}'.format(grouped_filename, command))
	print('show_grouped_report {report_name} -group {{group_name}} \
>> {filename}'.format(
		filename=grouped_filename, report_name=report_name))
	# report be model
	report_name='cmp_be_{model_type}_{model_A}-{model_B}'.format(
		model_type=model_type, model_A=model_A, model_B=model_B)
	command = 'report_{model_type}_model {model_A}-{model_B}_be \
-report {report_name} -group {{group_name}}'.format(
		model_type=model_type, model_A=model_A, model_B=model_B, 
		report_name=report_name)
	print(command)
	#print('echo {1} >> {0}'.format(raw_filename, '#'*80))
	print('echo {1} >> {0}'.format(raw_filename_be, command))
	print('show_report {report_name} >> {filename}'.format(
		filename=raw_filename_be, report_name=report_name))
		
	#print('echo {1} >> {0}'.format(grouped_filename, '#'*80))
	print('echo {1} >> {0}'.format(grouped_filename_be, command))
	print('show_grouped_report {report_name} -group {{group_name}} \
>> {filename}'.format(
		filename=grouped_filename_be, report_name=report_name))

write_scripts()