//
//	config.h
//	config.cpp
//
//	Format of configuration
//	KEY=VALUE
//	1)Any space in KEY and VALUE will be ignored.
//	2)The begining of KEY should be alphabetic or numeric word. 
//		Otherwise, the statement is ignored.
//	3)KEY is case sensitive.
//  4)Using ',' to seperate VALUE with multiple element.
//  5)Using the following syntax to set VALUE with multiple line.
//	multiple_line_config = {
//	...
//	}  
//		Every words in the line with end bracket will be ignored.
//
//	Example:
//	key.a = val
//   0key= 1, 2, 3
//	mlc = {
//		a
//	b c d
//	}
//	Comment:
//	#key=*
//     =XD
//	
//	v2.1.0 // new syntax for mu;tiple line and change parameter to case sensitive

#ifndef _CONFIG_H_
#define _CONFIG_H_
#include "common.h"

class config{
public:
	void readConfig(istream &is);
	void getCfg(const string &key, string &value);
	void getCfg(const string &key, bool &value);
	void getCfg(const string &key, int &value);
	void getCfg(const string &key, double &value);
	void getCfg(const string &key, UDT_STR_VEC &value);
	void getCfg(const string &key, UDT_SET<string> &value);
	void getCfg(const string &key, set<string> &value);
private:
	UDT_STR_STR_MAP m_table;
};

#endif
