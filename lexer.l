%option case-insensitive
%{
#define	YY_NO_UNISTD_H 1
#ifndef LINUX_ENV
	#include <io.h>
#else
	#include <unistd.h>
#endif
#include "struct.h"
#include "parser.tab.h"
extern int lineNum;
extern "C"{
	int yywrap(void);
	int yylex(void);
}
enum PARSE_STATE {PS_INIT};
PARSE_STATE g_parse_state = PS_INIT;
bool g_read_obj = false;
bool is_eof = false;
int level = 0;
%}
%x COMMENT_LINE COMMENT_BLOCK

mk_comment_line1	^"*"
mk_comment_line2	"$"
ws              [ \f\r\t\v]+
letter          [a-z]
digit           [0-9]
notation		[!#%@\/<>_\?\|\.\&:$]
operator		[+=-]
expression      '.*'
identifier		({operator}|{notation}|{letter}|{digit})+
word			({expression}|{identifier})
kw_r			^r/{identifier}
kw_c			^c/{identifier}
kw_d			^d/{identifier}
kw_m			^m/{identifier}
kw_x			^x/{identifier}
kw_subckt		".subckt"
kw_ends			".ends"
kw_option		".option"
kw_statment		^{ws}*\./{identifier}
newline			\n
contline		\+
error			.

%%

{mk_comment_line1}|{mk_comment_line2}	{
						//cout << "COMMENT_LINE " << yytext << endl;
						BEGIN COMMENT_LINE;
						int i = 0;
						while (yytext[i] != '\0') {
							if (yytext[i] == '\n')
								lineNum++;
							i++;
						}
					}
<COMMENT_LINE>{newline}	{
						BEGIN INITIAL;
						++lineNum;
					}
<COMMENT_LINE>.		;	/* do nothing inside single line comment */
{newline}			{
						//cout << "EOL:" << lineNum << endl;
						++lineNum;
						return EOL;
					}
<<EOF>>				{
						if(is_eof == false){
							//cout << "EOF:" << lineNum << endl;
							++lineNum;
							is_eof = true;
							return EOL;
						}else{
							return 0;
						}
					}
{contline}			{
						return CONT;
					}
{kw_subckt}			{
						//cout << "KW_SUBCKT " << yytext << endl;
						return KW_SUBCKT;
					}
{kw_ends}			{
						//cout << "KW_ENDS " << yytext << endl;
						return KW_ENDS;
					}
{kw_option}			{
						//cout << "KW_OPTION " << yytext << endl;
						return KW_OPTION;
					}
{kw_statment}		{
						//cout << "KW_STATMENT " << yytext << endl;
						return KW_STATMENT;
					}
{kw_r}				{
						//cout << "R" << endl;
						return KW_R;
					}
{kw_c}				{
						//cout << "C" << endl;
						return KW_C;
					}
{kw_d}				{
						//cout << "D" << endl;
						return KW_D;
					}
{kw_m}				{
						//cout << "M" << endl;
						return KW_M;
					}
{kw_x}				{
						//cout << "X" << endl;
						return KW_X;
					}
{word}				{
						//cout << "WORD " << yytext << endl;
						yylval.m_str = new std::string(yytext);
						return WORD;
					}
.					{
						//cout << lineNum << "ignore:" << yytext << endl;
					}

%%
int yywrap(void)
{
        return 1;
}