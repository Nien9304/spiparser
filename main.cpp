const char g_message_prefix[] = "[SpiceParser] ";
const char g_version[] = 
//
//	main.cpp
//	SpiParser
//
//	Created by PCCO	@2015/03/07
"1.3.5"//support @ in instance name
;
#define MEMORY_DEBUG 0
#if MEMORY_DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include "struct.h"
#include "parser.tab.h"

extern UDT_STR_SUBCKT_MAP g_subsktMap;
extern string g_option;
extern FILE *yyin;

UDT_SET<string> g_mosfetList;

void ReadCellList(const char* filename, vector<string> &cellList );
void FlatNetlist(const string &hierarchyName, UDT_ELEMENT_VEC &dstNetlist, 
	UDT_ELEMENT_VEC &srcNetlist, 
	UDT_STR_STR_MAP &nodeMap, UDT_STR_SUBCKT_MAP &subsktMap);
string SubstituteNodeName(const string &hierarchyName, 
	const string &srcName, const UDT_STR_STR_MAP &nodeMap);
void FlatAllNetlist(vector<string> &cellList, vector<subckt> &subcktList, 
	UDT_STR_SUBCKT_MAP &subsktMap);
void GenerateAllSpiceFile(string location, vector<string> &cellList, 
	vector<subckt> &subcktList);
void writeJson(ostream &os, vector<string> &cellList, UDT_SET<string> &g_mosfetList);

int main(int argc, char **argv){
#if MEMORY_DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_crtBreakAlloc = -1;
#endif
	vector<string> cellList;
	vector<subckt> subcktList;
	ofstream ofs;

	if(argc != 3){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./SpiceParser [working directory(./)] "
			<< "[Spice file path(*.spi/*.cir)]"  
			<< endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./" << endl;
		//cerr << "Files read from the working directory:" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./[cell].spips (spice netlist file)" << endl;
		cerr << "./result.json (result file)" << endl;
		exit(-1);
	}
	string path = argv[1];
	string filename = argv[2];
	yyin = fopen(filename.c_str(),"r");
	if( yyin == NULL ){
		cerr << "Error: Cannot open " << filename << endl;
		exit(-1);
	}
	cout << g_message_prefix << "Start parsing " << filename << endl;
    if(yyparse() == 1){
		cerr << "Parse error, abort!" << endl;
		exit(-1);
	}
	fclose(yyin);
	//ReadCellList(argv[3], cellList );
	//cout << g_message_prefix << "flat all netlist" << endl;
	//FlatAllNetlist(cellList, subcktList, g_subsktMap);
	//
	//This version disable the netlist flattening feature
	//dspf from calibre xRC is already flatted
	for(UDT_STR_SUBCKT_MAP::iterator subcktIt = g_subsktMap.begin(); 
		subcktIt != g_subsktMap.end(); ++subcktIt){
			cellList.push_back(subcktIt->first);
			subcktList.push_back(subcktIt->second);
	}
	cout << g_message_prefix << "Write cell spice netlist" << endl;
	GenerateAllSpiceFile(path + "/", cellList, subcktList);
	filename = path + "/" + "result.json";
	cout << g_message_prefix << "Write result to " << filename << endl;
	openFile(ofs, filename);
	writeJson(ofs, cellList, g_mosfetList);
	ofs.close();
	/*cout << g_message_prefix << "report MOSFET: " << endl;
	for(UDT_SET<string>::iterator strIt = g_mosfetList.begin();
		strIt != g_mosfetList.end(); ++strIt){
		cout << "   " << *strIt << endl;
	}*/
	return 0;
} 

void writeJson(ostream &os, vector<string> &cellList, UDT_SET<string> &g_mosfetList){
	bool flag = false;
	os << "{\"cell_list\":[" << endl;
	for(unsigned idx = 0; idx < cellList.size(); ++idx){
		if(flag) os << ",";
		os << "\"" <<  cellList[idx] << "\"" <<  endl;
		flag = true;
	}
	os << "], \"mosfet_list\":[" << endl;
	flag = false;
	for(UDT_SET<string>::iterator strIt = g_mosfetList.begin();
		strIt != g_mosfetList.end(); ++strIt){
		if(flag) os << ",";
		os << "\"" << *strIt << "\"" <<  endl;
		flag = true;
	}
	os << "]}" << endl;
}
void ReadCellList(const char* filename, vector<string> &cellList ){
	ifstream ifs;
	string str;
	openFile(ifs, filename);
	if(ifs.is_open() == false){
		cerr << "Error: Cannot open " << filename << endl;
		exit(-1);
	}
	cout << ">> start parsing " << filename << endl;
	while(!ifs.eof()){
		getline(ifs, str);
		if(!str.empty()){
			cellList.push_back(str);
		}
	}
	ifs.close();
}
string SubstituteNodeName(const string &hierarchyName, const string &srcName, 
const UDT_STR_STR_MAP &nodeMap){
	UDT_STR_STR_MAP::const_iterator nodeIt = nodeMap.find(srcName);
	if(nodeIt == nodeMap.end()){
		string buf = srcName;
		transform(buf.begin(), buf.end(), buf.begin(), ::tolower);
		if(buf == "0" || buf == "gnd" || buf == "gnd!" || buf == "ground"){
			return srcName;
		}
		else{
			return hierarchyName+srcName;
		}
	}
	else{
		return nodeIt->second;
	}
}
void FlatNetlist(const string &hierarchyName, UDT_ELEMENT_VEC &dstNetlist, 
UDT_ELEMENT_VEC &srcNetlist, UDT_STR_STR_MAP &nodeMap, UDT_STR_SUBCKT_MAP &subsktMap){
	for(UDT_ELEMENT_VEC::iterator elementIt = srcNetlist.begin(); 
	elementIt != srcNetlist.end(); ++elementIt){
		UDT_STR_STR_MAP subNodeMap;
		UDT_STR_SUBCKT_MAP::iterator subcktIt;
		switch(elementIt->m_type){
		case ET_X:
			//X name {pin} subckt
			subcktIt = subsktMap.find(elementIt->m_description.back());
			if(subcktIt == subsktMap.end()){
				/*cerr << "Error: Element X" << elementIt->m_description.back() 
					<< "is not found!" << endl;
				exit(-1);*/
				//UMC28 use subckt as MOS.
				//M name D G S B p/n ...
				dstNetlist.push_back(element());
				dstNetlist.back().m_type = ET_M;
				dstNetlist.back().m_description.resize(elementIt->m_description.size());
				dstNetlist.back().m_description[0] = string("X") + hierarchyName + 
					elementIt->m_description[0];
				for(int i=1; i<=4; ++i){
					dstNetlist.back().m_description[i] = 
						SubstituteNodeName(hierarchyName, 
						elementIt->m_description[i], nodeMap);
				}
				for(int i=5; i<dstNetlist.back().m_description.size(); ++i){
					dstNetlist.back().m_description[i] = elementIt->m_description[i];
				}
				g_mosfetList.insert(elementIt->m_description[5]);
			}else{
				//X			name	{pin}	subckt
				//.subckt	subckt	{pin}
				for(int i=1; i<elementIt->m_description.size()-1; ++i){ //map the pins
					subNodeMap[subcktIt->second.m_description[i]] = 
						elementIt->m_description[i];
				}
				FlatNetlist(
					hierarchyName + elementIt->m_description.front() + INSTANCE_DELIMITER, 
					dstNetlist, subcktIt->second.m_netlist, subNodeMap, subsktMap);
			}
			break;
		case ET_R: 
		case ET_C:
			//R name L R val {$comment ...}
			//C name L R val {$comment ...}
			dstNetlist.push_back(element());
			dstNetlist.back().m_type = elementIt->m_type;
			dstNetlist.back().m_description.resize(elementIt->m_description.size());
			dstNetlist.back().m_description[0] = hierarchyName + elementIt->m_description[0];
			dstNetlist.back().m_description[1] = 
				SubstituteNodeName(hierarchyName, elementIt->m_description[1], nodeMap);
			dstNetlist.back().m_description[2] = 
				SubstituteNodeName(hierarchyName, elementIt->m_description[2], nodeMap);
			dstNetlist.back().m_description[3] = elementIt->m_description[3];
			for(unsigned index = 4; index < elementIt->m_description.size(); ++index){
				dstNetlist.back().m_description[index] = 
					elementIt->m_description[index];
			}
			break;
		case ET_M:
			//M name D G S B p/n ...
			dstNetlist.push_back(element());
			dstNetlist.back().m_type = elementIt->m_type;
			dstNetlist.back().m_description.resize(elementIt->m_description.size());
			dstNetlist.back().m_description[0] = string("M") + hierarchyName + 
				elementIt->m_description[0];
			for(int i=1; i<=4; ++i){
				dstNetlist.back().m_description[i] = 
					SubstituteNodeName(hierarchyName, elementIt->m_description[i], nodeMap);
			}
			for(int i=5; i<dstNetlist.back().m_description.size(); ++i){
				dstNetlist.back().m_description[i] = elementIt->m_description[i];
			}
			g_mosfetList.insert(elementIt->m_description[5]);
			break;
		default:
			cerr << "Error: Element has unknown type!" << endl;
			exit(-1);
			break;
		}
	}
}
void FlatAllNetlist(vector<string> &cellList, vector<subckt> &subcktList,
UDT_STR_SUBCKT_MAP &subsktMap){
	for(vector<string>::iterator strIt = cellList.begin(); strIt != cellList.end(); ++strIt){
		UDT_STR_SUBCKT_MAP::iterator subcktIt = subsktMap.find(*strIt);
		UDT_STR_STR_MAP nodeMap;
		if(subcktIt == subsktMap.end()){
			cout << "Warning: " << *strIt 
				<< " is in the cell list but not found in the spice file." << endl;
			//exit(-1);
			strIt->clear();
			continue;
		}
		subcktList.push_back(subckt());
		subcktList.back().m_description = subcktIt->second.m_description;
		FlatNetlist("", subcktList.back().m_netlist, subcktIt->second.m_netlist, 
			nodeMap, subsktMap);
	}
}
void GenerateAllSpiceFile(string location, vector<string> &cellList, 
vector<subckt> &subcktList){
	ofstream ofs;
	string filename;
	for(int i=0; i<cellList.size(); ++i){
		if(cellList[i].empty()) continue;
		filename = location + cellList[i] + ".spips";
		openFile(ofs, filename);
		ofs << "#option" << SEP;
		if(g_option.empty()){
			ofs << "0" << endl;
		}else{
			ofs << "1" << endl;
			ofs << g_option << endl;
		}
		ofs << "#description" << SEP << subcktList[i].m_description.size();
		for(vector<string>::iterator strIt = subcktList[i].m_description.begin(); 
			strIt != subcktList[i].m_description.end(); ++strIt){
			ofs << SEP << *strIt;
		}
		ofs << endl;
		ofs << "#element" << SEP << subcktList[i].m_netlist.size() << endl;
		for(vector<element>::iterator elementIt = subcktList[i].m_netlist.begin();
		elementIt != subcktList[i].m_netlist.end(); ++elementIt){
			ofs << ELEMENT_TYPE_OUTPUT_NAME[elementIt->m_type] 
			<< SEP << elementIt->m_description.size() << SEP;
			switch(elementIt->m_type){
			case ET_M:
				ofs << 'M';
				break;
			case ET_X:
				ofs << 'X';
				break;
			}
			for(vector<string>::iterator strIt = elementIt->m_description.begin(); 
				strIt != elementIt->m_description.end(); ++strIt){
				ofs << *strIt << SEP;
			}
			ofs << endl;
		}
		ofs << "#EOF" << endl;
		ofs.close();
	}
}
