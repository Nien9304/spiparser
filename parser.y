%{
#define YYDEBUG 0
#include "struct.h"

int lineNum = 1;
UDT_STR_SUBCKT_MAP g_subsktMap;
string g_option; //single option
extern char* yytext;
extern "C"{
	void yyerror(const char *s);
	extern int yylex(void);
}

%}
%token KW_SUBCKT
%token KW_ENDS
%token KW_OPTION
%token KW_STATMENT
%token KW_R
%token KW_C
%token KW_D
%token KW_M
%token KW_X
%token EOL
%token CONT
%token <m_str>WORD
%token ERROR

%union {
	std::string *m_str;
	UDT_STR_VEC *m_vecstr;
	UDT_ELEMENT_VEC *m_elementVec;
	char m_char;
	int m_int;
};

%type <m_vecstr> statement;
%type <m_elementVec> netlist;

%%
file:
	file EOL{
	}
|	file KW_OPTION statement EOL{
		string option;
		for(int i=0; i<$3->size(); ++i){
				option += " ";
				option += (*$3)[i];
		}

		if(g_option.empty()){ //set option
			g_option.swap(option);
			cout << ">> Report option: " << g_option << endl;
		}
		else if(g_option != option){
			cerr << "Error: .option statement mismatch. This program only support single option." << endl;
			exit(-1);
		}
		/*
		cout << ">> Report option ";
		for(int i=0; i<$3->size(); ++i){
			 cout << (*$3)[i] << " ";
		}
		cout << endl;*/
		delete $3;
	}
|	file KW_SUBCKT statement EOL netlist KW_ENDS EOL{
		string &name =  $3->front();
		//cout << ">> subckt:" << name << endl;
		g_subsktMap[name] = subckt(*$3, *$5);

		delete $3;
		delete $5;
	}
|	{
	}
;
netlist:
	netlist EOL{
	}
|
	netlist KW_STATMENT statement EOL{
		$$ = $1;
		$$->push_back(element(ET_STAT, *$3));
		delete $3;
	}
|
	netlist KW_R statement EOL{
		$$ = $1;
		$$->push_back(element(ET_R, *$3));
		delete $3;
	}
|
	netlist KW_C statement EOL{
		$$ = $1;
		$$->push_back(element(ET_C, *$3));
		delete $3;
	}
|
	netlist KW_D statement EOL{
		$$ = $1;
		$$->push_back(element(ET_D, *$3));
		delete $3;
	}
|
	netlist KW_M statement EOL{
		$$ = $1;
		$$->push_back(element(ET_M, *$3));
		delete $3;
	}
|
	netlist KW_X statement EOL{
		$$ = $1;
		$$->push_back(element(ET_X, *$3));
		delete $3;
	}
|
	{
		$$ = new UDT_ELEMENT_VEC;
	}
;
statement:
	statement WORD{
		$$ = $1;
		$$->push_back(*$2);
		delete $2;
	}
|	statement EOL CONT{
		$$ = $1;
	}
|	{
		$$ = new UDT_STR_VEC;
	}
;

%%
void yyerror(const char *s){
	std::cerr<< "Error(line" << lineNum << "): " << s;
	std::cerr<< "\t token: " << yytext << std::endl;
}