#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"

extern "C" int yyparse (void);

enum ELEMENT_TYPE{ET_R, ET_C, ET_D, ET_M, ET_X, ET_STAT, ET_INVALID};
static const char *ELEMENT_TYPE_NAME[] = {
	"ET_R", "ET_C", "ET_D", "ET_M", "ET_X", "ET_STAT", "ET_INVALID"};
static const char *ELEMENT_TYPE_OUTPUT_NAME[] = {
	"ET_R", "ET_C", "ET_D", "ET_M", "ET_M", "ET_STAT", "ET_INVALID"};

struct element;
typedef vector<element> UDT_ELEMENT_VEC;
struct subckt;
typedef UDT_MAP<string, subckt> UDT_STR_SUBCKT_MAP;

struct element{
	element():m_type(ET_INVALID){};
	element(ELEMENT_TYPE type, UDT_STR_VEC description):m_type(type){
		m_description.swap(description);
	};

	ELEMENT_TYPE m_type;
	UDT_STR_VEC m_description;
	//R name L R val
	//C name L R val
	//M name D G S B p/n ...
	//X name {pin} subckt
};
struct subckt{
	subckt(){};
	subckt(UDT_STR_VEC &description, UDT_ELEMENT_VEC &netlist){
		m_description.swap(description);
		m_netlist.swap(netlist);
	};
	
	UDT_STR_VEC m_description;
	UDT_ELEMENT_VEC m_netlist;
};

#endif